from django.urls        import path
from django.contrib     import admin
from .                  import views

urlpatterns = [
    path(r'^register/$', views.register, name='register'),
    path(r'^login/$', views.user_login, name='login'),
    path(r'^logout/$', views.user_logout, name='logout'),
]
