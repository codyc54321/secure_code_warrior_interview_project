from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import generic
from django.urls import reverse

from .models import BankAccount
from .forms import BankAccountForm


class DetailView(generic.DetailView):
    model = BankAccount
    template_name = 'bank_accounts/detail.html'

def new_bank_account_created(request):
    return HttpResponse("Account created successfully!")

def new_bank_account(request):
    if request.method == 'POST':
        form = BankAccountForm(request.POST)
        if form.is_valid():
            new_account = form.save(commit=False)
            new_account.save()
            return redirect(reverse('bank_accounts:new-bank-account-created'))
    else:
        form = BankAccountForm()
    return render(request, 'bank_accounts/new_account.html', {'form': form})
