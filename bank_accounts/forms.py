from django import forms

from .models import BankAccount

class BankAccountForm(forms.ModelForm):

    class Meta:
        model = BankAccount
        fields = ('first_name', 'last_name', 'phone_number', )
