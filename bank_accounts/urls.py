from django.urls import path

from . import views

app_name = 'bank_accounts'
urlpatterns = [
    path('new-bank-account', views.new_bank_account, name='new-bank-account'),
    path('new-bank-account-created', views.new_bank_account_created, name='new-bank-account-created'),
]
