from django.db import models
from django.core.validators import RegexValidator

# https://stackoverflow.com/questions/19130942/whats-the-best-way-to-store-phone-number-in-django-models
phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")

CHECKING = 'CK'
SAVINGS = 'SV'
OTHER = 'OT'
BANK_ACCOUNT_TYPE_CHOICES = (
    (CHECKING, 'Checking'),
    (SAVINGS, 'Savings'),
    (OTHER, 'Other'),
)

class BankAccount(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True) # validators should be a list
    account_is_active = models.BooleanField(default=True)
    funds = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    account_type = models.CharField(choices=BANK_ACCOUNT_TYPE_CHOICES, max_length=2)
