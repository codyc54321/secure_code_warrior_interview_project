# Generated by Django 2.1.1 on 2018-09-06 00:30

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BankAccount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=250)),
                ('last_name', models.CharField(max_length=250)),
                ('phone_number', models.CharField(blank=True, max_length=17, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex='^\\+?1?\\d{9,15}$')])),
                ('account_is_active', models.BooleanField(default=True)),
                ('funds', models.DecimalField(decimal_places=2, default=0, max_digits=15)),
                ('account_type', models.CharField(choices=[('CK', 'Checking'), ('SV', 'Savings'), ('OT', 'Other')], max_length=2)),
            ],
        ),
    ]
